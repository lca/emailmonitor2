﻿// Desc: Email Monitor Add-In for FairFax County. Creates/modifys Viz FeedStreamer Feeds viz email triggers

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;

namespace EmailMonitor
{
    public class Logger
    {
        StreamWriter sWriter;
        string logPath;

        public Logger()
        {
            logPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
            //Debug.WriteLine("--- DEBUG --- " + logPath);
        }

        public void Log(string logMessage, Boolean store = true)
        {
            logMessage = DateTime.Now + " : " + logMessage;
            Debug.WriteLine(logMessage);

            if (store)
            {
                sWriter = File.AppendText(logPath + "\\viz_email_monitor_log.txt");
                sWriter.WriteLine(logMessage);
                sWriter.Close();
            }
        }
    }
    public partial class ThisAddIn
    {

        private volatile bool _resetEvents;
        Logger logr;
        public Outlook.Items items;
        public Outlook.NameSpace outlookNameSpace;
        public Outlook.MAPIFolder inbox;

        Boolean tickerOn = false, fullscreenOn = false, bgOn = false, emergencyState = false, upNextOn = false, tickerWasOn = false, pauseDaemon = false;
        private static System.Timers.Timer timer, dailyTimer;

        // Email filtration strings
        string email_subject_filter = "VIZ FEED STREAMER";
        string account_to_monitor = "lca_dev@outlook.com";//_sys-ffxems.sys-ffxems@fairfaxcounty.gov";
        
        static string feedStreamerInputDataPath = "C:\\VizTickerFeeds\\Fairfax\\";
        static string feedStreamerIniPath = "C:\\ProgramData\\Vizrt\\Viz Ticker Feed Settings\\";//Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Vizrt\\Viz Ticker Feed Settings\\";

        static string[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        // Triggered on addin start
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //Debug.WriteLine("--------------------- INSTANTIATED");
            logr = new Logger();
            logr.Log("ADDIN STARTED..");
            //logr.Log();

            _resetEvents = true;
            SetItemAddEvents();

            Thread eventMonitorThread = new Thread(this.MonitorExchangeConnection);
            eventMonitorThread.Start();

            Thread dailyRoutineThread = new Thread(this.DailyRoutine);
            dailyRoutineThread.Start();
        }

        // sets the item events
        private void SetItemAddEvents()
        {
            if (_resetEvents)
            {
                outlookNameSpace = this.Application.GetNamespace("MAPI");
                foreach (Outlook.Folder folder in outlookNameSpace.Folders) {
                    //logr.Log(folder.Name);
                    if (folder.Name.Equals(account_to_monitor)) {
                        foreach (Outlook.Folder innerFolder in folder.Folders) {
                            if (innerFolder.Name.Equals("Inbox"))
                            {
                                inbox = innerFolder;
                                //logr.Log("Item Count: " + folder.Items.Count);
                            }
                        }
                    }
                }
                _resetEvents = false;
            }
          
        }

        public void MonitorExchangeConnection()
        {
            Outlook.MailItem mailItem;
            while (true)
            {
                Thread.Sleep(500);
                _resetEvents = true;
                items = inbox.Items;

                foreach (object item in items)
                {
                    if (item is Outlook.MailItem)
                    {
                        mailItem = (Outlook.MailItem)item;
                        //new Logger().Log(mailItem.Subject);
                        if (mailItem.UnRead)
                        {
                            items_ItemAdd(mailItem);
                        }

                    }
                }
            }
        }

        // Action triggered when a new email arrives
        void items_ItemAdd(object Item)
        {
            logr.Log("New Email");
            Outlook.MailItem mail;
            try
            {
                string template = "fairfax_email", carousel = "EMERGENCY", headline = "ALERT", content = "", hostname = "localhost";

                if (Item is Outlook.MailItem)
                {
                    mail = (Outlook.MailItem)Item;
                    if (Item != null)
                    {
                        if (mail.MessageClass == "IPM.Note" && mail.Subject.ToUpper().Contains(email_subject_filter.ToUpper()))
                        {
                            if (mail.Attachments.Count > 0)
                            {
                                //logr.Log(mail.Attachments[1].DisplayName);
                                logr.Log("Found attachment, parsing..");
                                mail.Attachments[1].SaveAsFile(feedStreamerInputDataPath + mail.Attachments[1].FileName);
                                foreach (string line in System.IO.File.ReadAllLines(feedStreamerInputDataPath + mail.Attachments[1].FileName))
                                {
                                    content = line.Trim();
                                }

                                if (!content.Equals(""))
                                {
                                    logr.Log("Sending content message of: " + content);
                                    new Networker().SendTCP("send RENDERER*FUNCTION*DataPool*Data SET EMERGENCY_TEXT=\"\"" + content + "\"\"");

                                    PerformCustomCommand("STOP NEWS");
                                    PerformCustomCommand("STOP UP NEXT");
                                    PerformCustomCommand("STOP BAC");
                                    PerformCustomCommand("START EMERGENCY");
                                    PerformCustomCommand("TRIGGER EMERGENCY SOUND");
                                    
                                }
                                else
                                {
                                    logr.Log("Not creating/updating xml file.");
                                }

                            }
                            else
                            {
                                logr.Log("No attachments..");
                            }

                            // Parse for commands
                            foreach (string line in mail.Body.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
                            {
                                //logr.Log(line);
                                try
                                {
                                    if (!line.Trim().Equals(""))
                                    {
                                        this.PerformCustomCommand(line.Trim());
                                        logr.Log("Triggered command: " + line.Trim());
                                    }
                                }
                                catch (Exception e) { }
                            }

                            // Delete email
                            mail.Delete();
                        }
                        else
                        {
                            logr.Log("Email found but does not match rules for ticker feed.");
                            logr.Log("Subject: " + mail.Subject.ToUpper());
                            logr.Log("Sender: " + mail.SenderEmailAddress.ToUpper());
                            logr.Log("Attachments: " + mail.Attachments.Count);
                            logr.Log("Type: " + mail.MessageClass);
                        }


                    }
                }
            }
            catch (Exception e)
            {
                logr.Log("Error with email: " + e);
                //logr.Log();
            }
        }


        // Routine Background Thread
        private void DailyRoutine()
        {
            Boolean upNext = false;
            Boolean bac = false;
            Boolean updated = false;
            int lastMinuteRecorded = DateTime.Now.Minute;
            int lastMin = 0;

            while (true)
            {
                if (!pauseDaemon && !emergencyState)
                {
                    int curMin = DateTime.Now.Minute;

                    //new Logger().Log(DateTime.Now.ToString());
                    if (curMin - lastMin > 0)
                    {
                        new Logger().Log("Min: " + curMin, false);
                        lastMin = curMin;
                    }

                    if (curMin % 5 == 0 && !updated)
                    {
                        UpdateFiles();
                        updated = true;
                    }

                    if (curMin % 2 == 0 && updated)
                    {
                        updated = false;
                    }

                    // if UPNEXT is not visible but BAC is
                    if (!upNext && bac)
                    {
                        // if 3 minutes have passed
                        if (lastMinuteRecorded != -1 && (curMin - lastMinuteRecorded == 3))
                        {
                            lastMinuteRecorded = -1;

                            PerformCustomCommand("STOP BAC");
                            bac = false;

                            PerformCustomCommand("START NEWS");
                            new Logger().Log("STOPPING BAC");
                        }
                    }

                    // if we are on the 55th or 25th minute, display the next upcoming shows.

                    if ((curMin == 55 || curMin == 25) && !upNext)
                    {

                        if (bac)
                        {
                            PerformCustomCommand("STOP BAC");
                            bac = false;
                        }

                        TriggerUpNextEvent();
                        upNext = true;
                        lastMinuteRecorded = 0;
                        new Logger().Log("STARTING UPNEXT");
                    }

                    else if ((curMin == 0 || curMin == 58 || curMin == 28) && upNext)
                    {
                        TriggerReverseUpNextEvent();
                        upNext = false;
                        lastMinuteRecorded = curMin;
                        new Logger().Log("STOPPING UPNEXT");
                    }

                    // if running a regular session
                    if (!upNext && !bac)
                    {
                        if ((curMin - lastMinuteRecorded == 2))
                        {
                            PerformCustomCommand("START BAC");
                            bac = true;
                            new Logger().Log("STARTING BAC");
                            lastMinuteRecorded = curMin;
                        }
                    }
                }
            }
        }

        // Updates files with dynamic names
        public void UpdateFiles()
        {

            int curMonth = DateTime.Now.Month;

            // BAC
            try
            {
                StreamReader reader = new StreamReader(feedStreamerInputDataPath + "Public Meeting " + months[curMonth - 1] + ".txt");
                string input = reader.ReadToEnd();

                using (StreamWriter writer = new StreamWriter(feedStreamerInputDataPath + "bac.txt", false))
                {
                    {
                        writer.Write(input);
                    }
                    writer.Close();
                }
            }
            catch (Exception exc)
            {
                //new Logger().Log("File not found: " + feedStreamerInputDataPath + "Public Meeting " + months[curMonth - 1] + ".txt");

            }

            // UP NEXT
            try
            {
                StreamReader reader = new StreamReader(feedStreamerInputDataPath + "coming_up.txt");
                string input = reader.ReadToEnd();

                using (StreamWriter writer = new StreamWriter(feedStreamerInputDataPath + "up_next.txt", false))
                {
                    {
                        writer.Write("ignore_rnd:" + new Random().Next() + Environment.NewLine + input);
                    }
                    writer.Close();
                }
            }
            catch (Exception exc)
            {
                // Most likely the file was not found if we are here. Drop out of the function.
                //new Logger().Log("File not found: " + feedStreamerInputDataPath + "coming_up.prn");
            }
        }

        public static void TriggerUpNextEvent()
        {
            new Logger().Log("Triggering the upnext scroller..");
            //new Networker().SendTCP("send RENDERER*STAGE*DIRECTOR*CHECK_BAC STOP");
            new Networker().SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL CONTINUE REVERSE");
            new Networker().SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_BAC CONTINUE REVERSE");
            new Networker().SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_UPNEXT CONTINUE");

        }

        public static void TriggerReverseUpNextEvent()
        {
            new Logger().Log("Triggering (in reverse) the upnext scroller..");
            new Networker().SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL CONTINUE");
            new Networker().SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_UPNEXT CONTINUE REVERSE");
        }

        private void toggleTickerWasOn()
        {
            tickerWasOn = !tickerWasOn;
        }

        private void toggleTickerWasOnd()
        {
            tickerOn = !tickerOn;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            logr.Log("ADDIN SHUTTING DOWN");
        }

        
        public class Networker
        {
            Logger logr = new Logger();
            // send a tcp message
            public void SendTCP(string message)
            {
                Thread.Sleep(200);
                int messageByteLength = Encoding.ASCII.GetBytes(message).Length;
                //logr.Log("TCP Message Length: " + messageByteLength + " bytes");

                // if message is less then 2k MB send via UDP otherwise use TCP

                TcpClient server;
                byte[] resp = new byte[2048];
                try
                {
                    server = new TcpClient("127.0.0.1", 6100);
                }
                catch (SocketException se)
                {
                    logr.Log(se + "\n\nUnable to connect to server");
                    return;
                }
                NetworkStream ns = server.GetStream();
                if (message.Contains('^'))
                {
                    logr.Log("Response: ***********************************************\n\n" + message.Split('^')[1] + "\n*********************************************************\n\n");
                    logr.Log("    \"" + message.Split('^')[0] + "\" Response @ " + DateTime.UtcNow);
                    message = message.Split('^')[1];
                }


                if (!message.Equals(""))
                {
                    //logr.Log("Response: ***********************************************\n\n" + message + "\n*********************************************************\n\n");
                    byte[] bytesSent = Encoding.UTF8.GetBytes(message);
                    ns.Write(bytesSent, 0, bytesSent.Length);
                }
                ns.Flush();

                ns.Close();
                server.Close();

            }
        }

        void PerformCustomCommand(string line)
        {
            Networker ntwrkr = new Networker();
            // ======================================
            // ============= EMERGENCY ==============
            // ======================================
            if (line.Equals("START EMERGENCY"))
            {
                
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TEXT_INFO_FLIP_EMERGENCY START");
                //ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_EMERGENCY START");
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_EMERGENCY CONTINUE");
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*EMERGENCY_SCROLL START");
                
                emergencyState = true;
            }
            else if (line.Equals("STOP EMERGENCY"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_EMERGENCY CONTINUE REVERSE");
                emergencyState = false;
            }
            // ======================================
            // ============= NEWS ===================
            // ======================================
            else if (line.Equals("START NEWS"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL CONTINUE");
                tickerOn = true;
            }
            else if (line.Equals("STOP NEWS"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL CONTINUE REVERSE");
                tickerOn = false;
            }
            // ======================================
            // ============= UP NEXT ================
            // ======================================
            else if (line.Equals("START UP NEXT"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_UPNEXT CONTINUE");
                fullscreenOn = true;
            }
            else if (line.Equals("STOP UP NEXT"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_UPNEXT CONTINUE REVERSE");
                fullscreenOn = false;
            }
            // ======================================
            // ================ BAC =================
            // ======================================
            else if (line.Equals("START BAC"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_BAC CONTINUE");
                fullscreenOn = true;
            }
            else if (line.Equals("STOP BAC"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*TICKER_SCROLL_BAC CONTINUE REVERSE");
                fullscreenOn = false;
            }
            // ======================================
            // ============= JOB PROMO ==============
            // ======================================
            else if (line.Equals("START JOB PROMO"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*JOB_PROMO_FLIPPER CONTINUE");
                fullscreenOn = true;
            }
            else if (line.Equals("STOP JOB PROMO"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*JOB_PROMO_FLIPPER CONTINUE REVERSE");
                fullscreenOn = false;
            }
            // ======================================
            // ============= JOB NEWS ===============
            // ======================================
            else if (line.Equals("START JOB NEWS"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*JOB_NEWS_FLIPPER CONTINUE");
                fullscreenOn = true;
            }
            else if (line.Equals("STOP JOB NEWS"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*JOB_NEWS_FLIPPER CONTINUE REVERSE");
                fullscreenOn = false;
            }
            // ======================================
            // ============= BOARD AGENDA ===========
            // ======================================
            else if (line.Equals("START BOARD AGENDA"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*BOARD_AGENDA_PREVIEW_FLIPPER CONTINUE");
                fullscreenOn = true;
            }
            else if (line.Equals("STOP BOARD AGENDA"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*BOARD_AGENDA_PREVIEW_FLIPPER CONTINUE REVERSE");
                fullscreenOn = false;
            }
            // ======================================
            // ============= BG =====================
            // ======================================
            else if (line.Equals("START BG"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*BG CONTINUE");
                bgOn = true;
            }
            else if (line.Equals("STOP BG"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*BG CONTINUE REVERSE");
                bgOn = false;
            }
            else if (line.Equals("PAUSE SERVICE"))
            {
                pauseDaemon = true;
            }
            else if (line.Equals("CONTINUE SERVICE"))
            {
                pauseDaemon = false;
            }
            // ======================================
            // ============= SOUNDS =================
            // ======================================
            else if (line.Equals("TRIGGER AGENDA SOUND"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*BOARD_SOUND START");
            }
            else if (line.Equals("TRIGGER JOB OPPS SOUND"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*JOB_OPP_SOUND START");
            }
            else if (line.Equals("TRIGGER JOB NEWS SOUND"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*JOB_NEWS_SOUND START");
            }
            else if (line.Equals("TRIGGER EMERGENCY SOUND"))
            {
                ntwrkr.SendTCP("send RENDERER*STAGE*DIRECTOR*EMERGENCY_ALERT START");
            }
            // ======================================
            // ============= DEV TOOLS ==============
            // ======================================
            else if (line.Equals("DEV: TRIGGER UP NEXT EVENT"))
            {
                TriggerUpNextEvent();
            }
            else if (line.Equals("DEV: TRIGGER REVERSE UP NEXT EVENT"))
            {
                TriggerReverseUpNextEvent();
            }
        }

        void FindOrCreateTemplate(string template, string hostname="localhost")
        {
            // create ini file
            #region IniCreation
            try
            {
                using (StreamReader sr = new StreamReader(feedStreamerIniPath + "default.ini"))
                {
                    String line = sr.ReadToEnd();
                    
                    // update mseq host name
                    line = line.Replace("mseq-hostname=", "mseq-hostname=" + hostname);
                    // update source path
                    line = line.Replace("source=", "source=file:///" + feedStreamerInputDataPath.Replace('\\', '/').Replace(':', '|') + template + ".xml");
                    using (StreamWriter outfile = new StreamWriter(feedStreamerIniPath + template + ".ini"))
                    {
                        outfile.Write(line);
                    }
                }
            }
            catch (Exception e)
            {
                logr.Log("The file could not be read: " + e.Message);
            }
            #endregion
        }

        // HELPER FUNCTIONS
        static string ProgramFilesx86()
        {
            if (8 == IntPtr.Size || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion

       
    }
}
